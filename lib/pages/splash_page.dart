import 'dart:async';

import 'package:attendance_application/theme.dart';
import 'package:flutter/material.dart';
import 'package:attendance_application/pages/home_page.dart';

class SplashPage extends StatefulWidget {
  //const SplashPage({ Key? key }) : super(key: key);

  @override
  _SplashPageState createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {
  void iniState() {
    super.initState();
    splashscreenStart();
  }

  splashscreenStart() async {
    var duration = const Duration(seconds: 3);
    return Timer(duration, () {
      Navigator.push(context, MaterialPageRoute(builder: (context) => Home()));
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: cyanMainColor,
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
           
           Icon(
             Icons.school,
             size: 100.0,
             color: whiteColor,
           ),

           SizedBox(
             height: 24.0,
           ),

            Text("Codo App",
              style: TextStyle(
                color: whiteColor,
                fontWeight: FontWeight.bold,
                fontSize: 30.0,
              ),
            )
          ],
        )
      ),
    );
  }
}
